import 'dart:io';
import "dart:async";
import "dart:typed_data";
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:fyp_app/Controller/base.dart';
import 'package:fyp_app/Controller/soundEffectBase.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'dart:math';
import 'package:bordered_text/bordered_text.dart';
import 'package:quiver/async.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image/image.dart' as imglib;
import 'package:path_provider/path_provider.dart';

import 'package:fyp_app/i18n.dart';

class puzzle_game extends StatefulWidget {
  @override
  _puzzle_game createState() => _puzzle_game();
}

class _puzzle_game extends State<puzzle_game> {
  Base base = new Base();
  Color timeColor = Colors.white;
  int listLength = 25;
  int numOfCardPerRow = 5;
  int _start = 99999;
  int _current =0;
  bool start = false;
  List<int> answer = new List(25);
  List<Image> puzzle_data = new List(25);
  List<Image> temp = new List(25);
  _puzzle_game(){
    startTimer();
  }

  void startTimer() {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {
      if (!start) {
        setState(() {
          start = true;
        });
        start_game();
      }
    });
    sub.onDone((){

    });
  }

  void start_game(){
    Alert(
      context: context,
//      style: alertStyle,
//      type: AlertType.info,
      title: I18n.of(context).difficulty,
//          desc:
//          I18n.of(context).gameInstruction,
      buttons: [
        DialogButton(
          child: Text(
            I18n.of(context).game1Lv1,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () async{

            temp = await splitImage(3);
            setState(() {
              listLength = 9;
              numOfCardPerRow = 3;
              puzzle_data = temp;
            });
            Navigator.pop(context);
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            I18n.of(context).game1Lv2,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () async{
            temp = await splitImage(4);
            setState(() {
              listLength = 16;
              numOfCardPerRow = 4;
              puzzle_data = temp;
            });
            Navigator.pop(context);
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            I18n.of(context).game1Lv3,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () async{
            temp = await splitImage(5);
            setState(() {
              listLength = 25;
              numOfCardPerRow = 5;
              puzzle_data = temp;
            });
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
    ).show();
  }

  Future<List<Image>> splitImage(numItemEachRow) async{
    ByteData bytes = await rootBundle.load('assets/puzzle_photo.jpg');
    // convert image to image from image package
    Uint8List imageUint8List = bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes);
    imglib.Image image = imglib.decodeImage(imageUint8List.cast<int>());

    int x = 0, y = 0;
    int width = (image.width / numItemEachRow).round();
    int height = (image.height / numItemEachRow).round();

    // split image to parts
    List<imglib.Image> parts = List<imglib.Image>();
    for (int i = 0; i < numItemEachRow; i++) {
      for (int j = 0; j < numItemEachRow; j++) {
//        if (!(i == numItemEachRow-1 && j == numItemEachRow-1)){
          parts.add(imglib.copyCrop(image, x, y, width, height));
          x += width;
//        }
      }
      x = 0;
      y += height;
    }
//    ByteData blackImageAsset = await rootBundle.load('assets/black_image.jpg');
//    Uint8List blackImageUint8List = blackImageAsset.buffer.asUint8List(blackImageAsset.offsetInBytes, blackImageAsset.lengthInBytes);
//    imglib.Image blackImage = imglib.decodeImage(blackImageUint8List.cast<int>());
//    parts.add(imglib.copyCrop(blackImage, 0, 0, width, height));
    var rng = new Random();
    bool saved = false;
    int index = 0;
    // convert image from image package to Image Widget to display
    List<Image> output = List<Image>(parts.length);
    for (var img in parts) {
      saved = false;
      while(!saved){
        int numInt = rng.nextInt(parts.length);
        if (output[numInt]==null){
          output[numInt] = Image.memory(imglib.encodeJpg(img));
          answer[index] = numInt;
          index +=1;
          saved = true;
        }
      }
    }

    return output;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
//                  SizedBox(
//                    width: MediaQuery.of(context).size.width / 2,
//                    child: Text(
//                      I18n.of(context).timeLeft+"Testing",
//                      textAlign: TextAlign.left,
//                      style: TextStyle(
//                        fontSize: 30.0,
//                        color: timeColor,
//                        decoration: TextDecoration.none,
//                        decorationColor: Colors.black,
//                      ),
//                    ),
//                  ),
                  base.backButton(context,'/home'),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width ,
                    child: Text(
//                      I18n.of(context).timeLeft+"Testing",
                      "拼圖遊戲",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 60.0,
                        color: Colors.black,
                        decoration: TextDecoration.none,
                        decorationColor: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: GridView.builder(
                        itemCount: listLength,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: numOfCardPerRow),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                              onTap: () {

                              },
                              child: Container(
                                child: Center(
                                    child: puzzle_data[index],
                                ),
                                margin: EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                      Radius.circular(5.0)),
                                ),
                              ));
                        },
                      ))
                ],
              ),
            ],
          ),
        ),
      ),
      onWillPop: () async {
        Future.value(
            false); //return a `Future` with false value so this route cant be popped or closed.
      },
    );
  }
}
