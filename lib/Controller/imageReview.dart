import 'package:flutter/material.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'package:fyp_app/Controller/base.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bordered_text/bordered_text.dart';

class ImgReview extends StatefulWidget {
  @override
  _ImgReview createState() => _ImgReview();
}

class _ImgReview extends State<ImgReview> {
  Base base = new Base();
  List<dynamic> list = ['abc.png'];
  List<dynamic> exchanged = [0];
  int coins = globals.scores;
  String name = globals.name;
  _ImgReview() {
    getList();
  }

  void getList() async {
    await Request.reviewImg();
    setState(() {
      list = globals.small_photo_list;
      exchanged =globals.exchanged_list;
    });
  }

  Image is_exchanged(index){
    if (exchanged[index]==0){
      return Image.asset("assets/locked.png");
    }else{
      return Image.network(Request.getip()+"/storage/"+list[index]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          base.photobackgroundColume(),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  base.nameAndCoins(context, name, coins),
                  base.logoAndInfo(context,"/imgReview"),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(90),
                  ),
                  base.imageReviewButton(context),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(10),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width-58,
                          child: GridView.builder(
                            itemCount: list.length,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2),
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      globals.index = index;
                                    });
                                    Navigator.pushNamed(context, '/imgReviewDetail');
                                  },
                                  child: Container(
                                    child: Center(
//                                        child: Image.network(Request.getip()+"/"+list[index])
                                        child: is_exchanged(index)
                                    ),
                                    margin: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                                      color: Colors.amber[100],
                                    ),
                                  ));
                            },
                          ))
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
