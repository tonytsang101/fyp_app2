import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fyp_app/Service/validation.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;

import 'package:fyp_app/i18n.dart';

class Signup extends StatefulWidget {
  @override
  _Signup createState() => new _Signup();
}

class _Signup extends State<Signup> {
  static var _formKey = new GlobalKey<FormState>();

  Validations _validations = new Validations();
  String _userid,_name, _password, _passwordConfirmation, _phone;

  void validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      var data = {
        'username': _userid,
        'name': _name,
        'password': _password,
        'password_confirmation': _passwordConfirmation,
        'phone': _phone,
      };
      request(data);
    } else {
      print('資料不正確');
    }
  }

  void request(data) async {
    try {
      Alert(
        context: context,
        type: AlertType.success,
        title: I18n.of(context).login,
        desc: await Request.requestSignUp(data),
        buttons: [
          DialogButton(
            child: Text(
              I18n.of(context).confirm,
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              globals.audioCache.play(globals.audioAsset[4]);
              Navigator.pop(context);
              Navigator.pop(context);
            },
            width: 120,
          )
        ],
      ).show();
    } catch (e) {
      print(e);
    }
  }

  Widget radioButton(bool isSelected) => Container(
        width: 16.0,
        height: 16.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: Colors.black)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              )
            : Container(),
      );

  Widget horizontalLine() => Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          width: ScreenUtil.getInstance().setWidth(120),
          height: 1.0,
          color: Colors.black26.withOpacity(.2),
        ),
      );

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance =
        ScreenUtil(width: 900, height: 1334, allowFontScaling: true);
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Image.asset("assets/login_bg.jpg"),
              ),
              Expanded(
                child: Container(),
              ),
//              Image.asset("assets/image_02.png")
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/logo.jpg",
                        width: ScreenUtil.getInstance().setWidth(210),
                        height: ScreenUtil.getInstance().setHeight(110),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(180),
                  ),
                  Container(
                    width: double.infinity,
                    height: ScreenUtil.getInstance().setHeight(1180),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12,
                              offset: Offset(0.0, 15.0),
                              blurRadius: 15.0),
                          BoxShadow(
                              color: Colors.black12,
                              offset: Offset(0.0, -10.0),
                              blurRadius: 10.0),
                        ]),
                    child: Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                      child:Form(
                        key: _formKey,
                        autovalidate: true,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(I18n.of(context).registration,
                                style: TextStyle(
                                    fontSize: ScreenUtil.getInstance().setSp(45),
                                    fontFamily: "Poppins-Bold",
                                    letterSpacing: .6)),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(30),
                            ),
                            Text(I18n.of(context).memberId,
                                style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: ScreenUtil.getInstance().setSp(26))),
                            TextFormField(
                                decoration: InputDecoration(
                                    hintText: I18n.of(context).memberId,
                                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                                validator: (value) {
                                  return _validations.validateUsername(value);
                                },
                                onSaved: (String userid) {
                                  _userid = userid;
                                }
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(30),
                            ),
                            Text(I18n.of(context).password,
                                style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: ScreenUtil.getInstance().setSp(26))),
                            TextFormField(
                                obscureText: true,
                                decoration: InputDecoration(
                                    hintText: I18n.of(context).password,
                                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                                validator: (value) {
                                  _password = value;
                                  return _validations.validatePassword(value);
                                },
                                onSaved: (String password) {
                                  _password = password;
                                }
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(30),
                            ),
                            Text(I18n.of(context).confirmPassword,
                                style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: ScreenUtil.getInstance().setSp(26))),
                            TextFormField(
                                obscureText: true,
                                decoration: InputDecoration(
                                    hintText: I18n.of(context).confirmPassword,
                                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                                validator: (value) {
                                  return _validations.validatePasswordConfirmation(value, _password);
                                },
                                onSaved: (String passwordConfirmation) {
                                  _passwordConfirmation = passwordConfirmation;
                                }
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(30),
                            ),
                            Text(I18n.of(context).name,
                                style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: ScreenUtil.getInstance().setSp(26))),
                            TextFormField(
                                decoration: InputDecoration(
                                    hintText: I18n.of(context).name,
                                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                                validator: (value) {
                                  return _validations.validateName(value);
                                },
                                onSaved: (String name) {
                                  _name = name;
                                }
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(30),
                            ),
                            Text(I18n.of(context).contactNum,
                                style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: ScreenUtil.getInstance().setSp(26))),
                            TextFormField(
                                decoration: InputDecoration(
                                    hintText: I18n.of(context).contactNum,
                                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                                validator: (value) {
                                  return _validations.validatePhone(value);
                                },
                                onSaved: (String phone) {
                                  _phone = phone;
                                }
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(35),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(40)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 12.0,
                          ),
                        ],
                      ),
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setWidth(330),
                          height: ScreenUtil.getInstance().setHeight(100),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xFF17ead9),
                                Color(0xFF6078ea)
                              ]),
                              borderRadius: BorderRadius.circular(6.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 8.0)
                              ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                globals.audioCache.play(globals.audioAsset[4]);
                                validateAndSave();
                                },
                              child: Center(
                                child: Text(I18n.of(context).confirm,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins-Bold",
                                        fontSize: 18,
                                        letterSpacing: 1.0)),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.getInstance().setHeight(50),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(50),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
