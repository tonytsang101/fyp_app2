import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

List<dynamic> photo_list = new List();
List<dynamic> small_photo_list = new List();
List<dynamic> exchanged_list = new List();
int scores = 0;
String name = "";
int index = 0;
int lockedPhotos = 0;
bool isZh = true;

AudioCache audioCache = AudioCache();
AudioPlayer audioPlayer = AudioPlayer(mode: PlayerMode.LOW_LATENCY);

AudioCache audioCache2 = AudioCache();
AudioPlayer audioPlayer2 = AudioPlayer(mode: PlayerMode.LOW_LATENCY);

List<String> audioAsset = [
  "audios/countdown.mp3",
  "audios/gamebackground.mp3",
  "audios/correct.mp3",
  "audios/wrong.mp3",
  "audios/buttonClick.mp3",
  "audios/gamebackground2.mp3",
];

