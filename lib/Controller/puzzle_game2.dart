import 'dart:io';
import "dart:async";
import "dart:typed_data";
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:fyp_app/Controller/base.dart';
import 'package:fyp_app/Controller/soundEffectBase.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'dart:math';
import 'package:bordered_text/bordered_text.dart';
import 'package:quiver/async.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image/image.dart' as imglib;
import 'package:path_provider/path_provider.dart';

import 'package:fyp_app/i18n.dart';

class puzzle_game2 extends StatefulWidget {
  @override
  _puzzle_game2 createState() => _puzzle_game2();
}

class _puzzle_game2 extends State<puzzle_game2> {
  Base base = new Base();
  Color timeColor = color[0];
  int listLength = 25;
  int numOfCardPerRow = 5;
  int numOfAnsPerRow = 4;
  int width_control = 0;

  int _start = 99999;
  int _current = 0;
  bool start = false;
  bool checking = false;
  static List<dynamic> color = [
    Colors.white,
    Colors.yellow[300],
    Colors.blue[300],
    Colors.green[500],
    Colors.purple[200],
    Colors.black,
    Colors.red[500],
  ];

  List<Widget> button_list1 = new List();
  List<Widget> button_list2 = new List();

  List<dynamic> question_color_list =
      new List.generate(25, (i) => i % 2 == 0 ? color[1] : color[2]);
  List<dynamic> question_border_color_list = new List.filled(25, color[0]);
  List<dynamic> ans_border_color_list = new List.filled(8, color[0]);
  List<dynamic> ans_background_color_list = new List.filled(8, color[0]);
  List<String> question_list = new List.filled(25, "");
  List<String> actual_question_list = new List.filled(25, "");
  List<String> equation_list = new List(20);
  List<String> symbol_list = ["+", "-", "x", "="];
  List<String> ans_list = new List.filled(8, "");
  List<int> ans_index_list = new List.filled(8, 0);
  List<bool> ans_disabled = new List.filled(8, false);

  int selected_ans_index = null;
  int selected_question_index = null;
  int question_type;
  int equation1 = 0;
  int equation2 = 5;
  int equation3 = 10;
  int equation4 = 15;
  int score = 0;
  int num_of_blanks = 0;
  int level = 1;
  int coins = 0;

  _puzzle_game2() {
    startTimer();
  }

  void rules(sub, levelDesc) async {
    String desc = levelDesc;
    Alert(
      context: context,
//      type: AlertType.info,
      title: I18n.of(context).gameRule,
      desc: desc,
      buttons: [
        DialogButton(
          child: Text(
            I18n.of(context).start,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            globals.audioCache.play(globals.audioAsset[4]);
            setState(() {
              sub.cancel();
              _start = 60;
              timeColor = Colors.black;
              startTimer();
            });
            soundEffectBase.puzzleGameBackgroundPlay();
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
    ).show();
  }

  void startTimer() {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {
      if (!start) {
        setState(() {
          start = true;
        });
        start_game(sub);
      }else{
        if (_current==6){
          globals.audioCache.play(globals.audioAsset[0]);
          setState(() {
            timeColor = Colors.red;
          });
        }
      }
      setState(() {
        _current = _start - duration.elapsed.inSeconds;
      });
    });
    sub.onDone(() {
      switch(level){
        case 1:
          if (score >= 15) {
            coins = 12;
          } else if (score >= 10) {
            coins = 9;
          } else if (score >= 5) {
            coins = 6;
          } else {
            coins = 3;
          }
          break;
        case 2:if (score >= 40) {
          coins = 20;
        } else if (score >= 25) {
          coins = 15;
        } else if (score >= 10) {
          coins = 10;
        } else {
          coins = 5;
        }
        break;
        case 3:if (score >= 50) {
          coins = 40;
        } else if (score >= 30) {
          coins = 30;
        } else if (score >= 15) {
          coins = 20;
        } else {
          coins = 10;
        }
        break;
        default: break;
      }

      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (_) => new Dialog(
            child: new Container(
              alignment: FractionalOffset.center,
              height: 200.0,
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          I18n.of(context).timesUp,
                          style: TextStyle(
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              height: 2,
                              letterSpacing: 1.0)
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          I18n.of(context).youHaveGot+"$coins"+I18n.of(context).coins,
                          style: TextStyle(
                              fontFamily: "Poppins-Bold",
                              fontSize: 20,
                              height: 2,
                              letterSpacing: 1.0)
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setWidth(230),
                          height: ScreenUtil.getInstance().setHeight(100),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xFF17ead9),
                                Color(0xFF6078ea)
                              ]),
                              borderRadius: BorderRadius.circular(6.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 8.0)
                              ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () async {
                                globals.audioCache.play(globals.audioAsset[4]);
                                globals.scores += coins;
                                Request.updateScores(globals.scores);
                                Navigator.pushNamedAndRemoveUntil(
                                    context, '/home', ModalRoute.withName('/'));
                              },
                              child: Center(
                                child: Text(I18n.of(context).collect,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins-Bold",
                                        fontSize: 18,
                                        letterSpacing: 1.0)),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ));
      sub.cancel();
    });

  }

  void start_game(sub) {
    Alert(
      context: context,
//      style: alertStyle,
//      type: AlertType.info,
      title: I18n.of(context).difficulty,
//          desc:
//          I18n.of(context).gameInstruction,
      buttons: [
        DialogButton(
          child: Text(
            I18n.of(context).game1Lv1,
            style: TextStyle(color: color[0], fontSize: 20),
          ),
          onPressed: () async {
            setState(() {
              level = 1;
              num_of_blanks = 2;
              numOfAnsPerRow = 4;
              width_control = 0;
              set_question();
            });
            Navigator.pop(context);
            rules(sub, I18n.of(context).puzzleGameInstruction1);
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            I18n.of(context).game1Lv2,
            style: TextStyle(color: color[0], fontSize: 20),
          ),
          onPressed: () async {
            setState(() {
              level = 2;
              num_of_blanks = 4;
              numOfAnsPerRow = 3;
              width_control = 195;
              set_question();
            });
            Navigator.pop(context);
            rules(sub, I18n.of(context).puzzleGameInstruction2);
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            I18n.of(context).game1Lv3,
            style: TextStyle(color: color[0], fontSize: 20),
          ),
          onPressed: () async {
            setState(() {
              level = 3;
              num_of_blanks = 6;
              numOfAnsPerRow = 4;
              width_control = 145;
              set_question();
            });
            Navigator.pop(context);
            rules(sub, I18n.of(context).puzzleGameInstruction3);

          },
          width: 120,
        )
      ],
    ).show();
  }



  String random_int(int range, int add) {
    var rng = new Random();
    return (rng.nextInt(range) + add).toString();
  }

  String add(String a, String b) {
    return (int.parse(a) + int.parse(b)).toString();
  }

  String minus(String a, String b) {
    return (int.parse(a) - int.parse(b)).toString();
  }

  String times(String a, String b) {
    return (int.parse(a) * int.parse(b)).toString();
  }

  void reset_question_list() {
    for (int i = 0; i < 25; i++) {
      question_list[i] = "";
    }
  }

  void perform_symbol(String symbol, int index) {
    switch (symbol) {
      case "+":
        equation_list[index + 4] =
            add(equation_list[index], equation_list[index + 2]);
        break;
      case "-":
        equation_list[index + 4] =
            minus(equation_list[index], equation_list[index + 2]);
        break;
      case "x":
        equation_list[index + 4] =
            times(equation_list[index], equation_list[index + 2]);
        break;
      default:
    }
  }

  void perform_symbol2(String symbol, int index) {
    switch (symbol) {
      case "+":
        equation_list[index + 2] =
            minus(equation_list[index + 4], equation_list[index]);
        if (int.parse(equation_list[index + 2]) < 0) {
          equation_list[index + 1] = symbol_list[1];
          equation_list[index + 2] = equation_list[index + 2].substring(1);
        }
        break;
      case "-":
        equation_list[index + 2] =
            minus(equation_list[index], equation_list[index + 4]);
        if (int.parse(equation_list[index + 2]) < 0) {
          equation_list[index + 1] = symbol_list[0];
          equation_list[index + 2] = equation_list[index + 2].substring(1);
        }
        break;
      default:
    }
  }

  void put_equation_in_list(
      int start_Index, int position_preset, String preset_int) {
    switch (position_preset) {
      case 0:
        equation_list[start_Index] = random_int(9, 1);
        equation_list[start_Index + 1] = symbol_list[0];
        equation_list[start_Index + 2] = random_int(9, 1);
        perform_symbol(equation_list[start_Index + 1], start_Index);
        break;
      case 1:
        equation_list[start_Index] = preset_int;
        equation_list[start_Index + 1] =
            symbol_list[int.parse(random_int(3, 0))];
        equation_list[start_Index + 2] = random_int(9, 1);
        perform_symbol(equation_list[start_Index + 1], start_Index);
        break;
      case 2:
        equation_list[start_Index] = random_int(9, 1);
        equation_list[start_Index + 1] =
            symbol_list[int.parse(random_int(3, 0))];
        equation_list[start_Index + 2] = preset_int;
        perform_symbol(equation_list[start_Index + 1], start_Index);
        break;
      case 3:
        equation_list[start_Index] = random_int(9, 1);
        equation_list[start_Index + 1] =
            symbol_list[int.parse(random_int(2, 0))];
        equation_list[start_Index + 4] = preset_int;
        perform_symbol2(equation_list[start_Index + 1], start_Index);
        break;
      default:
    }
    equation_list[start_Index + 3] = symbol_list[3];
  }

  void generate_equation(String position1, String position2, String position3) {
    if (position1 == null && position2 == null && position3 == null) {
      put_equation_in_list(equation1, 0, null);
    }
    if (position1 != null) {
      put_equation_in_list(equation2, question_type, position1);
    }
    if (position2 != null) {
      put_equation_in_list(equation3, question_type, position2);
    }
    if (position3 != null) {
      put_equation_in_list(equation4, question_type, position3);
    }
  }

  void put_equation_right(int equation_start_index, int ans_start_index) {
    for (int i = 0; i < 5; i++) {
      question_list[ans_start_index + i] =
          equation_list[equation_start_index + i];
    }
  }

  void put_equation_down(int equation_start_index, int ans_start_index) {
    for (int i = 0; i < 5; i++) {
      question_list[ans_start_index + i * 5] =
          equation_list[equation_start_index + i];
    }
  }

  void equation_to_question_list(int right) {
    if (right == 1) {
      switch (question_type) {
        case 1:
          put_equation_down(equation1, 0);
          put_equation_right(equation2, 0);
          put_equation_right(equation3, 10);
          put_equation_right(equation4, 20);
          break;
        case 2:
          put_equation_down(equation1, 2);
          put_equation_right(equation2, 0);
          put_equation_right(equation3, 10);
          put_equation_right(equation4, 20);
          break;
        case 3:
          put_equation_down(equation1, 4);
          put_equation_right(equation2, 0);
          put_equation_right(equation3, 10);
          put_equation_right(equation4, 20);
          break;
        default:
      }
    } else {
      switch (question_type) {
        case 1:
          put_equation_right(equation1, 0);
          put_equation_down(equation2, 0);
          put_equation_down(equation3, 2);
          put_equation_down(equation4, 4);
          break;
        case 2:
          put_equation_right(equation1, 10);
          put_equation_down(equation2, 0);
          put_equation_down(equation3, 2);
          put_equation_down(equation4, 4);
          break;
        case 3:
          put_equation_right(equation1, 20);
          put_equation_down(equation2, 0);
          put_equation_down(equation3, 2);
          put_equation_down(equation4, 4);
          break;
        default:
      }
    }
    copy_question();
  }

  void copy_question() {
    for (int i = 0; i < 25; i++) {
      actual_question_list[i] = question_list[i];
    }
  }

  void reset_question_color() {
    for (int i = 0; i < 25; i++) {
      if (i % 2 == 0) {
        question_color_list[i] = color[1];
      } else {
        question_color_list[i] = color[2];
      }
    }
  }

  void clear_null_color() {
    for (int i = 0; i < 25; i++) {
      if (question_list[i] == "") {
        question_color_list[i] = color[0];
      }
    }
  }

  void save_ans() {
    var rng = new Random();
    bool pass = false;
    int count = 0;
    for (int i = 0; i < num_of_blanks; i++) {
      while (!pass) {
        int index = rng.nextInt(25);
        if (question_list[index] != "" && question_list[index] != "=") {
          ans_list[i] = question_list[index];
          question_list[index] = "";
          ans_index_list[count] = index;
          pass = true;
        }
      }
      count++;
      pass = false;
    }

    for (int i = num_of_blanks; i < num_of_blanks + 2; i++) {
      int fake_ans = rng.nextInt(25) + 1;
      ans_list[i] = fake_ans.toString();
    }

    for (int i = 0; i < num_of_blanks + 2; i++) {
      ans_border_color_list[i] = color[4];
      ans_background_color_list[i] = color[4];
      String temp_ans;
      int temp_index;
      int target_index = rng.nextInt(num_of_blanks + 2);
      temp_ans = ans_list[i];
      temp_index = ans_index_list[i];
      ans_list[i] = ans_list[target_index];
      ans_list[target_index] = temp_ans;
      ans_index_list[i] = ans_index_list[target_index];
      ans_index_list[target_index] = temp_index;
    }
    ans_to_button_list();
  }

  void ans_to_button_list() {
    button_list1.clear();
    button_list2.clear();
    for (int i = 0; i < num_of_blanks + 2; i++) {
      if (i <= num_of_blanks ~/ 2) {
        button_list1.add(ans_flat_button(i));
      } else {
        button_list2.add(ans_flat_button(i));
      }
    }
  }

  Widget ans_flat_button(i) => FlatButton(
        child: Text(
          ans_list[i],
          style: TextStyle(
            fontSize: 40.0,
          ),
        ),
        color: ans_border_color_list[i],
        textColor: color[5],
        onPressed: ans_disabled[i]? null : () {
          selected_ans(i);
          check_ans();
        },
      );

  void set_question() async {
    setState(() {
      reset_question_list();
      reset_question_color();
      question_type = int.parse(random_int(3, 1));
      generate_equation(null, null, null);
      generate_equation(equation_list[0], equation_list[2], equation_list[4]);
      equation_to_question_list(int.parse(random_int(2, 1)));
      clear_null_color();
      save_ans();
    });
  }

  void selected_question(index) {
    if (checking!=true){
      for (int i = 0; i < num_of_blanks; i++) {
        question_border_color_list[ans_index_list[i]] = color[0];
      }
      if (question_list[index] == "" && question_color_list[index] != color[0]) {
        setState(() {
          selected_question_index = index;
          question_border_color_list[index] = color[3];
        });
      }
    }
  }

  void reset_ans_border_color() {
    for (int i = 0; i < ans_list.length; i++) {
      setState(() {
        ans_border_color_list[i] = color[4];
        ans_to_button_list();
      });
    }
  }

  void selected_ans(index) {
    if (!checking){
      reset_ans_border_color();
      setState(() {
        selected_ans_index = index;
        ans_border_color_list[index] = color[3];
        ans_to_button_list();
      });
    }
  }

  void check_ans() async {
    if (checking != true) {
      checking = true;
      if (selected_ans_index != null && selected_question_index != null) {
        if (question_color_list[selected_question_index] == color[3]) {
          return;
        }
        question_list[selected_question_index] = ans_list[selected_ans_index];

        if (actual_question_list[selected_question_index] ==
            ans_list[selected_ans_index]) {
          setState(() {
            ans_disabled[selected_ans_index] = true;
            ans_to_button_list();
            question_color_list[selected_question_index] = color[3];
            score += 1*level;
          });
          globals.audioCache.play(globals.audioAsset[2]);
          await new Future.delayed(const Duration(milliseconds: 1000));
          check_finish();
        } else {
          globals.audioCache.play(globals.audioAsset[3]);
          var temp = question_color_list[selected_question_index];
          for (int i = 0; i < 3; i++) {
            setState(() {
              question_color_list[selected_question_index] = color[6];
              ans_border_color_list[selected_ans_index] = color[6];
              ans_to_button_list();
            });
            await new Future.delayed(const Duration(milliseconds: 300));
            setState(() {
              question_color_list[selected_question_index] = temp;
              reset_ans_border_color();
            });
            await new Future.delayed(const Duration(milliseconds: 300));
          }
          if (score > level-1) {
            setState(() {
              score -= level-1;
            });
          } else {
            setState(() {
              score = 0;
            });
          }
          setState(() {
            question_list[selected_question_index] = "";
          });
        }
        setState(() {
          question_border_color_list[selected_question_index] = color[0];
          reset_ans_border_color();
          selected_ans_index = null;
          selected_question_index = null;
        });
      }
      checking = false;
    }
  }

  void reset_disable_ans(){
    for (int i = 0; i<8;i++){
      ans_disabled[i] = false;
    }
  }

  void check_finish() {
    int count = 0;
    for (int i = 0; i < 25; i++) {
      if (question_color_list[i] == color[3]) {
        count++;
      }
    }
    if (count == num_of_blanks) {
      reset_disable_ans();
      set_question();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Text(
                      I18n.of(context).timeLeft + "$_current",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 25.0,
                        color: timeColor,
                        decoration: TextDecoration.none,
                        decorationColor: color[5],
                      ),
                    ),
                  ),
                  base.backButton(context, '/home'),
                ],
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                        child: BorderedText(
                      strokeWidth: 1.0,
                      child: Text(
                        I18n.of(context).score + "$score",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 35.0,
                          color: color[5],
//                        decoration: TextDecoration.none,
//                        decorationColor: color[5],
                        ),
                      ),
                    )),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height - 250,
                      width: MediaQuery.of(context).size.width,
                      child: GridView.builder(
                        itemCount: listLength,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: numOfCardPerRow),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                              onTap: () {
                                selected_question(index);
                                check_ans();
                              },
                              child: Container(
                                child: Center(
                                  child: Text(
                                    question_list[index],
                                    style: TextStyle(
                                      fontSize: 50.0,
//                                      color: wordcolor_list[index],
                                      decoration: TextDecoration.none,
                                      decorationColor: color[5],
                                    ),
                                  ),
                                ),
                                margin: EdgeInsets.all(0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: question_border_color_list[index],
                                      width: 3),
                                  borderRadius: new BorderRadius.all(
                                      Radius.circular(5.0)),
                                  color: question_color_list[index],
                                ),
                              ));
                        },
                      ))
                ],
              ),
              ButtonBar(
                alignment: MainAxisAlignment.center,
                buttonHeight: 55,
                buttonMinWidth: 80,
                buttonPadding: EdgeInsets.only(left: 6, right: 6),
                children: button_list1,
              ),
              ButtonBar(
                alignment: MainAxisAlignment.center,
                buttonHeight: 55,
                buttonMinWidth: 80,
                buttonPadding: EdgeInsets.only(left: 6, right: 6),
                children: button_list2,
              ),
            ],
          ),
        ),
      ),
      onWillPop: () async {
        Future.value(
            false); //return a `Future` with false value so this route cant be popped or closed.
      },
    );
  }
}
