import "dart:async";
import 'package:flutter/material.dart';
import 'package:fyp_app/Controller/base.dart';
import 'package:fyp_app/Controller/soundEffectBase.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'dart:math';
import 'package:bordered_text/bordered_text.dart';
import 'package:quiver/async.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


import 'package:fyp_app/i18n.dart';

class Matching_game extends StatefulWidget {
  @override
  _Matching_game createState() => _Matching_game();
}

class _Matching_game extends State<Matching_game> {
  Base base = new Base();
  int level = 3;
  int listLength = 30;
  int numOfCardPerRow = 5;
  int minusScore = 5;
  int addScore = 1;
  int count = 0;
  List<dynamic> num_list = new List.filled(30, 0);
  List<dynamic> color = [
    Colors.white,
    Colors.lightGreenAccent,
    Colors.redAccent,
    Colors.red[500],
    Colors.orange[500],
    Colors.yellow[500],
    Colors.green[500],
    Colors.green[800],
    Colors.blue[500],
    Colors.blue[800],
    Colors.purple[500],
    Colors.purple[300],
    Colors.blueGrey[500],
  ];
  List<dynamic> color_list = new List(30);
  List<dynamic> wordcolor_list = new List(30);
  String ans = '0';
  int _start = 999;
  int _current = 0;
  int score = 0;
  int coins = 0;
  int firstAnsIndex = 0;
  Color ans_color;
  Color timeColor = Colors.white;
  bool checking = false;
  bool isFirstAns = true;
  bool start = false;
  var alertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: true,
    descStyle: TextStyle(fontWeight: FontWeight.bold),
    animationDuration: Duration(milliseconds: 400),
  );

  _Matching_game() {
    get_lists();
    startTimer();
  }

  void rules(sub,levelDesc)async{
    String desc = levelDesc;
    Alert(
      context: context,
      style: alertStyle,
//      type: AlertType.info,
      title: I18n.of(context).gameRule,
      desc:
      desc,
      buttons: [
        DialogButton(
          child: Text(
            I18n.of(context).start,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            globals.audioCache.play(globals.audioAsset[4]);
            setState(() {
              sub.cancel();
              _start = 60;
              timeColor = Colors.black;
              startTimer();
            });
            soundEffectBase.gameBackgroundPlay();
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
    ).show();
  }

  void startTimer() {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {

      if (!start) {
        setState(() {
          start = true;
        });
        Alert(
          context: context,
          style: alertStyle,
//      type: AlertType.info,
          title: I18n.of(context).difficulty,
//          desc:
//          I18n.of(context).gameInstruction,
          buttons: [
            DialogButton(
              child: Text(
                I18n.of(context).game1Lv1,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                globals.audioCache.play(globals.audioAsset[4]);
                Navigator.pop(context);
                setState(() {
                  level =1;
                  numOfCardPerRow = 3;
                  listLength = 12;
                  addScore = 1;
                  minusScore = 0;
                  get_lists();
                });
                rules(sub,I18n.of(context).matchingGameInstructionA);
              },
              width: 120,
            ),
            DialogButton(
              child: Text(
                I18n.of(context).game1Lv2,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                globals.audioCache.play(globals.audioAsset[4]);
                Navigator.pop(context);
                setState(() {
                  level =2;
                  numOfCardPerRow = 4;
                  listLength = 20;
                  addScore = 2;
                  minusScore = 1;
                  get_lists();
                });
                rules(sub,I18n.of(context).matchingGameInstructionB);
              },
              width: 120,
            ),
            DialogButton(
              child: Text(
                I18n.of(context).game1Lv3,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                globals.audioCache.play(globals.audioAsset[4]);
                Navigator.pop(context);
                setState(() {
                  level =3;
                  numOfCardPerRow = 5;
                  listLength = 30;
                  addScore = 3;
                  minusScore = 2;
                  get_lists();
                });
                rules(sub,I18n.of(context).matchingGameInstructionC);
              },
              width: 120,
            )
          ],
        ).show();

      }else{
        if (_current==6){
          globals.audioCache.play(globals.audioAsset[0]);
          setState(() {
            timeColor = Colors.red;
          });
        }
      }
      setState(() {
        _current = _start - duration.elapsed.inSeconds;
      });
    });
    sub.onDone(() {
      switch(level){
        case 1:
          if (score >= 15) {
            coins = 12;
          } else if (score >= 10) {
            coins = 9;
          } else if (score >= 5) {
            coins = 6;
          } else {
            coins = 3;
          }
          break;
        case 2:if (score >= 40) {
          coins = 20;
        } else if (score >= 25) {
          coins = 15;
        } else if (score >= 10) {
          coins = 10;
        } else {
          coins = 5;
        }
        break;
        case 3:if (score >= 50) {
          coins = 40;
        } else if (score >= 30) {
          coins = 30;
        } else if (score >= 15) {
          coins = 20;
        } else {
          coins = 10;
        }
        break;
        default: break;
      }

      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (_) => new Dialog(
            child: new Container(
              alignment: FractionalOffset.center,
              height: 200.0,
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          I18n.of(context).timesUp,
                          style: TextStyle(
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              height: 2,
                              letterSpacing: 1.0)
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          I18n.of(context).youHaveGot+"$coins"+I18n.of(context).coins,
                          style: TextStyle(
                              fontFamily: "Poppins-Bold",
                              fontSize: 20,
                              height: 2,
                              letterSpacing: 1.0)
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setWidth(230),
                          height: ScreenUtil.getInstance().setHeight(100),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xFF17ead9),
                                Color(0xFF6078ea)
                              ]),
                              borderRadius: BorderRadius.circular(6.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 8.0)
                              ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () async {
                                globals.audioCache.play(globals.audioAsset[4]);
                                globals.scores += coins;
                                Request.updateScores(globals.scores);
                                Navigator.pushNamedAndRemoveUntil(
                                    context, '/home', ModalRoute.withName('/'));
                              },
                              child: Center(
                                child: Text(I18n.of(context).collect,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins-Bold",
                                        fontSize: 18,
                                        letterSpacing: 1.0)),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ));
      sub.cancel();
    });
  }

  void reset_list() {
    for (int i = 0; i < listLength; i++) {
      num_list[i] = 0;
      wordcolor_list[i] = color[0];
    }
  }

  void reset_num(int index1, int index2) {
    var rng = new Random();
    Color colorRan;
    int numInt = 0;
    int numIndex = 0;
    bool accept = false;

    while(!accept){
      numIndex = rng.nextInt(listLength);
      if (numIndex != index1 && numIndex != index2){
        accept = true;
      }
    }
    numInt = num_list[numIndex];
    colorRan = color_list[numIndex];
    setState(() {
      num_list[index1] = numInt;
      color_list[index1] = colorRan;
    });
    numInt = rng.nextInt(30)+1;
    colorRan = color_list[rng.nextInt(10)+3];
    setState(() {
      num_list[index2] = numInt;
      color_list[index2] = colorRan;
    });
  }

  void reset_wordColor(){
    for (int i = 0; i < listLength; i++) {
      wordcolor_list[i] = color[0];
    }
  }

  void get_lists() {
    reset_list();
    bool accept = false;
    int numIndex = 0;
    int colorInt = 3;
    var rng = new Random();

    for (int i = 0; i < listLength/2; i++) {
      int numInt = rng.nextInt(30) + 1;
      colorInt += 1;
      if (colorInt >= color.length) {
        colorInt = 3;
      }
      for (int j = 0; j < 2; j++) {
        while (!accept) {
          numIndex = rng.nextInt(listLength);
          if (num_list[numIndex] == 0) {
            num_list[numIndex] = numInt;
            color_list[numIndex] = color[colorInt];
            accept = true;
          }
        }
        accept = false;
      }
    }
  }

  void checkAns2(num, color, index) async {
    setState(() {
      count +=1;
    });
    if (count<=2){
      if (!checking) {
        if (isFirstAns) {
          isFirstAns = false;
          setState(() {
            wordcolor_list[index] = this.color[1];
            firstAnsIndex = index;
          });
        } else {
          isFirstAns = true;
          if (num_list[index] == num_list[firstAnsIndex] &&
              color_list[index] == color_list[firstAnsIndex] &&
              index != firstAnsIndex) {
            setState(() {
              wordcolor_list[index] = this.color[1];
              score += addScore;
            });
            globals.audioCache.play(globals.audioAsset[2]);
            await new Future.delayed(const Duration(milliseconds: 1000));
            setState(() {
              reset_num(firstAnsIndex,index);
              reset_wordColor();
            });
          } else {
            globals.audioCache.play(globals.audioAsset[3]);
            for (int i = 0; i < 3; i++) {
              setState(() {
                wordcolor_list[index] = this.color[0];
                checking = true;
              });
              await new Future.delayed(const Duration(milliseconds: 300));
              setState(() {
                wordcolor_list[index] = this.color[2];
              });
              await new Future.delayed(const Duration(milliseconds: 300));
            }
            if (score > minusScore) {
              setState(() {
                score -= minusScore;
              });
            } else {
              setState(() {
                score = 0;
              });
            }
            setState(() {
              checking = false;
              reset_wordColor();

            });
          }
          setState(() {
            count =0;
          });
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Text(
                      I18n.of(context).timeLeft+"$_current",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 30.0,
                        color: timeColor,
                        decoration: TextDecoration.none,
                        decorationColor: Colors.black,
                      ),
                    ),
                  ),
                  base.backButton(context,'/home'),
                ],
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                        child: BorderedText(
                          strokeWidth: 1.0,
                          child: Text(
                            I18n.of(context).score+"$score",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 50.0,
                              color: Colors.black,
//                        decoration: TextDecoration.none,
//                        decorationColor: Colors.black,
                            ),
                          ),
                        )),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: GridView.builder(
                        itemCount: listLength,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: numOfCardPerRow),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                              onTap: () {
                                checkAns2(num_list[index].toString(),
                                    color_list[index], index);
                              },
                              child: Container(
                                child: Center(
                                    child: BorderedText(
                                      strokeWidth: 4.0,
                                      child: Text(
                                        num_list[index].toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 50.0,
                                          color: wordcolor_list[index],
                                          decoration: TextDecoration.none,
                                          decorationColor: Colors.black,
                                        ),
                                      ),
                                    )),
                                margin: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                      Radius.circular(5.0)),
                                  color: color_list[index],
                                ),
                              ));
                        },
                      ))
                ],
              ),
            ],
          ),
        ),
      ),
      onWillPop: () async {
        Future.value(
            false); //return a `Future` with false value so this route cant be popped or closed.
      },
    );
  }
}
