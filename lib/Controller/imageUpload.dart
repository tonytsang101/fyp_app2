import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
//import 'package:image_picker/image_picker.dart';
import 'package:fyp_app/Service/request.dart';


class ImageUpload extends StatefulWidget {
  ImageUpload() : super();

  @override
  _ImageUpload createState() => _ImageUpload();
}

class _ImageUpload extends State<ImageUpload> {
  //

  Future<File> file;
  String status = '';
  String base64Image;
  File tmpFile;
  String errMessage = '上傳錯誤 請重新上傳...';

  chooseImage() {
    setState(() {
//      file = ImagePicker.pickImage(source: ImageSource.gallery,maxHeight: 1024, maxWidth: 1024);
    });
    setStatus('');
  }

  setStatus(String message) {
    setState(() {
      status = message;
    });
  }

  startUpload() {
    setStatus('相片上傳中...');
    if (null == tmpFile) {
      setStatus(errMessage);
      return;
    }
    String fileName = tmpFile.path.split('/').last;
    request(fileName);
  }

  request(String fileName) async{
    try {
      Request.uploadImg(base64Image);
      imageCache.clear();
    } catch (e) {
      print(e);
    }
    Navigator.pushNamedAndRemoveUntil(context, '/ImgReview', (_) => false);
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: file,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            null != snapshot.data) {
          tmpFile = snapshot.data;
          base64Image = base64Encode(snapshot.data.readAsBytesSync());
          return Flexible(
            child: Image.file(
              snapshot.data,
              fit: BoxFit.fitWidth,
            ),
          );
        } else if (null != snapshot.error) {
          return const Text(
            '揀選圖片錯誤 請重新選擇...',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text(
            '未有已選相片',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("上傳相片"),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            OutlineButton(
              onPressed: chooseImage,
              child: Text('從相簿中揀選相片...'),
            ),
            SizedBox(
              height: 20.0,
            ),
            showImage(),
            SizedBox(
              height: 20.0,
            ),
            OutlineButton(
              onPressed: startUpload,
              child: Text('上傳'),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              status,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.green,
                fontWeight: FontWeight.w500,
                fontSize: 20.0,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}