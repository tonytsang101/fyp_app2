import 'package:fyp_app/Controller/globalVariable.dart' as globals;

class soundEffectBase {
  static void gameBackgroundPlay() async {
    globals.audioPlayer = await globals.audioCache.play(globals.audioAsset[1]);
  }

  static void puzzleGameBackgroundPlay() async {
    globals.audioPlayer = await globals.audioCache.play(globals.audioAsset[5]);
  }

  static void gameBackgroundStop() {
    globals.audioPlayer.stop();
  }

  static void gameCountdownPlay() async {
    globals.audioPlayer2 = await globals.audioCache.play(globals.audioAsset[1]);
  }
  static void gameCountdownStop() {
    globals.audioPlayer2.stop();
  }
}
