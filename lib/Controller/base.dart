import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'package:fyp_app/Controller/soundEffectBase.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:fyp_app/i18n.dart';

class Base {
  Widget backgroundColume() => Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Image.asset("assets/login_bg.jpg"),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      );

  Widget photobackgroundColume() => Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Image.asset("assets/photo_bg.jpg"),
          ),
          Expanded(
            child: Container(),
          ),
//              Image.asset("assets/image_02.png")
        ],
      );

  Widget logoAndInfo(context,currentLocation) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image.asset(
            "assets/logo.jpg",
            width: ScreenUtil.getInstance().setWidth(210),
            height: ScreenUtil.getInstance().setHeight(110),
          ),
          languageButton(context,currentLocation)
        ],
      );

  Widget nameAndCoins(context, name, coins) => Text(I18n.of(context).hello+"$name"+I18n.of(context).coinsShow+"$coins");

  Widget imageReviewButton(context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[getPresent(context), backButton(context, '/home')],
      );

  Widget imageReviewDetailButton(context,download_link) => Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[ downloadButton(context, download_link),backButton(context,'/imgReview')],
  );

  var alertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: true,
    descStyle: TextStyle(fontWeight: FontWeight.bold),
    animationDuration: Duration(milliseconds: 400),
  );

  Widget getPresent(context) => InkWell(
        child: Container(
          width: ScreenUtil.getInstance().setWidth(230),
          height: ScreenUtil.getInstance().setHeight(80),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xFF17ead9), Color(0xFF6078ea)]),
              borderRadius: BorderRadius.circular(6.0),
              boxShadow: [
                BoxShadow(
                    color: Color(0xFF6078ea).withOpacity(.3),
                    offset: Offset(0.0, 8.0),
                    blurRadius: 8.0)
              ]),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () async {
                globals.audioCache.play(globals.audioAsset[4]);
                Request.unlockImg(globals.lockedPhotos);
                Alert(
                  context: context,
                  style: alertStyle,
//      type: AlertType.info,
                  title: I18n.of(context).rewards,
                  desc: I18n.of(context).getRewardsCompleted,
                  buttons: [
                    DialogButton(
                      child: Text(
                        I18n.of(context).confirm,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      onPressed: () {
                        globals.audioCache.play(globals.audioAsset[4]);
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/home', (_) => false);
                      },
                      width: 120,
                    )
                  ],
                ).show();
              },
              child: Center(
                child: Text(I18n.of(context).getRewards,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Poppins-Bold",
                        fontSize: 18,
                        letterSpacing: 1.0)),
              ),
            ),
          ),
        ),
      );

  Widget backButton(context, route) => InkWell(
        child: Container(
          width: ScreenUtil.getInstance().setWidth(230),
          height: ScreenUtil.getInstance().setHeight(80),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xFF17ead9), Color(0xFF6078ea)]),
              borderRadius: BorderRadius.circular(6.0),
              boxShadow: [
                BoxShadow(
                    color: Color(0xFF6078ea).withOpacity(.3),
                    offset: Offset(0.0, 8.0),
                    blurRadius: 8.0)
              ]),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () async {
                globals.audioCache.play(globals.audioAsset[4]);
                soundEffectBase.gameBackgroundStop();
                soundEffectBase.gameCountdownStop();
                Navigator.pushNamedAndRemoveUntil(context, route, (_) => false);
              },
              child: Center(
                child: Text(I18n.of(context).back,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Poppins-Bold",
                        fontSize: 18,
                        letterSpacing: 1.0)),
              ),
            ),
          ),
        ),
      );

  Widget downloadButton(context,downloadLink) => InkWell(
    child: Container(
      width: ScreenUtil.getInstance().setWidth(230),
      height: ScreenUtil.getInstance().setHeight(80),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xFF17ead9), Color(0xFF6078ea)]),
          borderRadius: BorderRadius.circular(6.0),
          boxShadow: [
            BoxShadow(
                color: Color(0xFF6078ea).withOpacity(.3),
                offset: Offset(0.0, 8.0),
                blurRadius: 8.0)
          ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () async {
            globals.audioCache.play(globals.audioAsset[4]);
            Request.downloadImg(downloadLink);
          },
          child: Center(
            child: Text(I18n.of(context).download,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Poppins-Bold",
                    fontSize: 18,
                    letterSpacing: 1.0)),
          ),
        ),
      ),
    ),
  );

  void changeLanguage(){
    if (globals.isZh){
      I18n.load( Locale("en"));
      globals.isZh = false;
    }else{
      I18n.load( Locale("zh"));
      globals.isZh = true;
    }
  }

  Widget languageButton(context, currentLocation) => InkWell(
    child: Container(
      width: ScreenUtil.getInstance().setWidth(80),
      height: ScreenUtil.getInstance().setHeight(80),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xFF17ead9), Color(0xFF6078ea)]),
          borderRadius: BorderRadius.circular(6.0),
          boxShadow: [
            BoxShadow(
                color: Color(0xFF6078ea).withOpacity(.3),
                offset: Offset(0.0, 8.0),
                blurRadius: 8.0)
          ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () async {
            globals.audioCache.play(globals.audioAsset[4]);
            changeLanguage();
            Navigator.pushNamedAndRemoveUntil(
                context, currentLocation, (_) => false);
          },
          child: Center(
            child: Text(I18n.of(context).language,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Poppins-Bold",
                    fontSize: 18,
                    letterSpacing: 1.0)),
          ),
        ),
      ),
    ),
  );
}
