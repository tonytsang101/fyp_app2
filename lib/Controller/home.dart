import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fyp_app/Controller/base.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;

import 'package:fyp_app/i18n.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  int coins = 0;
  String name = "";
  @override
  void initState() {
    super.initState();
    updateUserInfo();
  }

  void updateUserInfo() async{
    await Request.getUserInfo();
    await this.setState(() {
      coins = globals.scores;
      name = globals.name;
    });
  }

  Base base = new Base();
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance =
        ScreenUtil(width: 750, height: 1334, allowFontScaling: true);
    return WillPopScope(
      child:  Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: true,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            base.backgroundColume(),
            SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
                child: Column(
                  children: <Widget>[
                    base.nameAndCoins(context, name, coins),
                    base.logoAndInfo(context,"/home"),
                    SizedBox(
                      height: ScreenUtil.getInstance().setHeight(180),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        InkWell(
                          child: Container(
                            width: ScreenUtil.getInstance().setWidth(330),
                            height: ScreenUtil.getInstance().setHeight(100),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xFF17ead9),
                                  Color(0xFF6078ea)
                                ]),
                                borderRadius: BorderRadius.circular(6.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFF6078ea).withOpacity(.3),
                                      offset: Offset(0.0, 8.0),
                                      blurRadius: 8.0)
                                ]),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () async {
                                  globals.audioCache.play(globals.audioAsset[4]);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, '/', (_) => false);
                                },
                                child: Center(
                                  child: Text(I18n.of(context).logout,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Poppins-Bold",
                                          fontSize: 18,
                                          letterSpacing: 1.0)),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 80.0),
                    ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//
//                      InkWell(
//                        child: Container(
//                          width: ScreenUtil.getInstance().setWidth(330),
//                          height: ScreenUtil.getInstance().setHeight(100),
//                          decoration: BoxDecoration(
//                              gradient: LinearGradient(colors: [
//                                Color(0xFF17ead9),
//                                Color(0xFF6078ea)
//                              ]),
//                              borderRadius: BorderRadius.circular(6.0),
//                              boxShadow: [
//                                BoxShadow(
//                                    color: Color(0xFF6078ea).withOpacity(.3),
//                                    offset: Offset(0.0, 8.0),
//                                    blurRadius: 8.0)
//                              ]),
//                          child: Material(
//                            color: Colors.transparent,
//                            child: InkWell(
//                              onTap: () async {
//                                Navigator.pushNamedAndRemoveUntil(
//                                    context, '/game1', (_) => false);
//                              },
//                              child: Center(
//                                child: Text("遊戲1",
//                                    style: TextStyle(
//                                        color: Colors.white,
//                                        fontFamily: "Poppins-Bold",
//                                        fontSize: 18,
//                                        letterSpacing: 1.0)),
//                              ),
//                            ),
//                          ),
//                        ),
//                      )
//                    ],
//                  ),
//                  Padding(
//                    padding: EdgeInsets.only(top: 20.0),
//                  ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        InkWell(
                          child: Container(
                            width: ScreenUtil.getInstance().setWidth(330),
                            height: ScreenUtil.getInstance().setHeight(100),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xFF17ead9),
                                  Color(0xFF6078ea)
                                ]),
                                borderRadius: BorderRadius.circular(6.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFF6078ea).withOpacity(.3),
                                      offset: Offset(0.0, 8.0),
                                      blurRadius: 8.0)
                                ]),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () async {
                                  globals.audioCache.play(globals.audioAsset[4]);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, '/game1', (_) => false);
                                },
                                child: Center(
                                  child: Text(I18n.of(context).matchingGame+" 1",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Poppins-Bold",
                                          fontSize: 18,
                                          letterSpacing: 1.0)),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        InkWell(
                          child: Container(
                            width: ScreenUtil.getInstance().setWidth(330),
                            height: ScreenUtil.getInstance().setHeight(100),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xFF17ead9),
                                  Color(0xFF6078ea)
                                ]),
                                borderRadius: BorderRadius.circular(6.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFF6078ea).withOpacity(.3),
                                      offset: Offset(0.0, 8.0),
                                      blurRadius: 8.0)
                                ]),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () async {
                                  globals.audioCache.play(globals.audioAsset[4]);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, '/game2', (_) => false);
                                },
                                child: Center(
                                  child: Text(I18n.of(context).matchingGame+" 2",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Poppins-Bold",
                                          fontSize: 18,
                                          letterSpacing: 1.0)),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        InkWell(
                          child: Container(
                            width: ScreenUtil.getInstance().setWidth(330),
                            height: ScreenUtil.getInstance().setHeight(100),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xFF17ead9),
                                  Color(0xFF6078ea)
                                ]),
                                borderRadius: BorderRadius.circular(6.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFF6078ea).withOpacity(.3),
                                      offset: Offset(0.0, 8.0),
                                      blurRadius: 8.0)
                                ]),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () async {
                                  globals.audioCache.play(globals.audioAsset[4]);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, '/puzzleGame2', (_) => false);
                                },
                                child: Center(
                                  child: Text(I18n.of(context).puzzleGame,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Poppins-Bold",
                                          fontSize: 18,
                                          letterSpacing: 1.0)),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        InkWell(
                          child: Container(
                            width: ScreenUtil.getInstance().setWidth(330),
                            height: ScreenUtil.getInstance().setHeight(100),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xFF17ead9),
                                  Color(0xFF6078ea)
                                ]),
                                borderRadius: BorderRadius.circular(6.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFF6078ea).withOpacity(.3),
                                      offset: Offset(0.0, 8.0),
                                      blurRadius: 8.0)
                                ]),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () async {
                                  globals.audioCache.play(globals.audioAsset[4]);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, '/imgReview', (_) => false);
                                },
                                child: Center(
                                  child: Text(I18n.of(context).myPhoto,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Poppins-Bold",
                                          fontSize: 18,
                                          letterSpacing: 1.0)),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          content: Text(I18n.of(context).leave),
          actions: [
            FlatButton(
              child: Text(I18n.of(context).yes),
              onPressed: () => Navigator.pop(c, true),
            ),
            FlatButton(
              child: Text(I18n.of(context).no),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
    );
  }
}
