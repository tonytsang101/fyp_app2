import 'package:flutter/material.dart';
import 'package:fyp_app/Service/request.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'package:fyp_app/Controller/base.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:photo_view/photo_view.dart';

class ImgReviewDetail extends StatefulWidget {
  @override
  _ImgReviewDetail createState() => _ImgReviewDetail();
}

class _ImgReviewDetail extends State<ImgReviewDetail> {
  Base base = new Base();
  String image_path = "";

  _ImgReviewDetail() {
    image_path = globals.photo_list[globals.index];
  }

  void getImagePath() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          base.photobackgroundColume(),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  base.logoAndInfo(context,"/imgReviewDetail"),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(140),
                  ),
                  base.imageReviewDetailButton(context,image_path.substring(0, image_path.length-4)),
                  SizedBox(
                    height: ScreenUtil.getInstance().setHeight(60),
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        height: ScreenUtil.getInstance().setHeight(700),
                        width: MediaQuery.of(context).size.width-58,
                        child: PhotoView(
                          imageProvider: NetworkImage(
                            Request.getip() + "/storage/" + image_path,
                          ),
                          minScale: PhotoViewComputedScale.contained * 0.8,
                          maxScale: PhotoViewComputedScale.covered * 2,
//                          enableRotation: true,
                        backgroundDecoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
