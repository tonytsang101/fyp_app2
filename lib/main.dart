import 'package:flutter/material.dart';
import 'package:fyp_app/Controller/login.dart';
import 'package:fyp_app/Controller/puzzle_game.dart';
import 'package:fyp_app/Controller/puzzle_game2.dart';
import 'package:fyp_app/Controller/signup.dart';
import 'package:fyp_app/Controller/home.dart';
import 'package:fyp_app/Controller/imageUpload.dart';
import 'package:fyp_app/Controller/imageReview.dart';
import 'package:fyp_app/Controller/imageReviewDetail.dart';
import 'package:fyp_app/Controller/matching_game.dart';
import 'package:fyp_app/Controller/matching_game2.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;

import 'package:fyp_app/i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctxt) {
    return new MaterialApp(
      localizationsDelegates: [
        const I18nDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: I18nDelegate.supportedLocals,
      locale: Locale("zh"),
      initialRoute: '/',
      routes: {
        '/': (context) => Login(),
        '/signup': (context) => Signup(),
        '/home': (context) => Home(),
        '/imgUpload': (context) => ImageUpload(),
        '/imgReview': (context) => ImgReview(),
        '/imgReviewDetail': (context) => ImgReviewDetail(),
        '/game1': (context) => Matching_game(),
        '/game2': (context) => Matching_game2(),
        '/puzzleGame': (context) => puzzle_game(),
        '/puzzleGame2': (context) => puzzle_game2(),
      },
    );
  }
}
void main() => runApp(MyApp());