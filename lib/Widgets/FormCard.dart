import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:fyp_app/Service/validation.dart';
import 'package:fyp_app/Service/request.dart';



class FormCardLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      height: ScreenUtil.getInstance().setHeight(500),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, -10.0),
                blurRadius: 10.0),
          ]),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("登入",
                style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(45),
                    fontFamily: "Poppins-Bold",
                    letterSpacing: .6)),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("會員編號",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextField(
              decoration: InputDecoration(
                  hintText: "會員編號",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("密碼",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: "密碼",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(35),
            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                Text(
//                  "Forgot Password?",
//                  style: TextStyle(
//                      color: Colors.blue,
//                      fontFamily: "Poppins-Medium",
//                      fontSize: ScreenUtil.getInstance().setSp(28)),
//                )
//              ],
//            )
          ],
        ),
      ),
    );
  }
}

class FormCardSignup extends StatelessWidget {
  static var _formKey = new GlobalKey<FormState>();
  Validations _validations = new Validations();
  static String _userid,_name, _password, _passwordConfirmation, _phone;





  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      height: ScreenUtil.getInstance().setHeight(950),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, -10.0),
                blurRadius: 10.0),
          ]),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("註冊",
                style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(45),
                    fontFamily: "Poppins-Bold",
                    letterSpacing: .6)),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("會員編號",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextFormField(
              decoration: InputDecoration(
                  hintText: "會員編號",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                validator: (value) {
                  return _validations.validateUsername(value);
                },
                onSaved: (String userid) {
                  _userid = userid;
                }
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("密碼",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: "密碼",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                validator: (value) {
                  _password = value;
                  return _validations.validatePassword(value);
                },
                onSaved: (String password) {
                  _password = password;
                }
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("確認密碼",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: "確認密碼",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                validator: (value) {
                  return _validations.validatePasswordConfirmation(value, _password);
                },
                onSaved: (String passwordConfirmation) {
                  _passwordConfirmation = passwordConfirmation;
                }
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("姓名",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: "姓名",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                validator: (value) {
                  return _validations.validateName(value);
                },
                onSaved: (String name) {
                  _name = name;
                }
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text("聯絡電話",
                style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: "電話",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                validator: (value) {
                  return _validations.validatePhone(value);
                },
                onSaved: (String phone) {
                  _phone = phone;
                }
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(35),
            ),
          ],
        ),
      ),
    );
  }
}