import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fyp_app/Controller/globalVariable.dart' as globals;
import 'package:url_launcher/url_launcher.dart';


class Request {
  static String getip(){
    return 'http://192.168.0.101:8082';
  }

  static Future<bool> requestLogIn(data) async {
    final CREATE_POST_URL = getip()+'/api/auth/login';

    Response response;
    Dio dio = new Dio();
    response = await dio.post(CREATE_POST_URL, data: data);
    if (response.data['message'] != 'Unauthorized') {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('access_token', response.data['access_token']);
      prefs.setString('token_type', 'Bearer');
      return true;
    } else
      return false;
  }

  static Future<String> requestSignUp(data) async {
    final CREATE_POST_URL = getip()+'/api/auth/signup';
    Response response;
    Dio dio = new Dio();
    // dio.options.connectTimeout = 10000; 
    // dio.options.receiveTimeout=3000;
    response = await dio.post(CREATE_POST_URL, data: data);
    return response.data['message'];
  }

  static void uploadImg(base64Image) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String CREATE_UPLOAD_IMG_URL =
        getip()+'/api/auth/upimg';
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.connectTimeout = 10000; //5s
      dio.options.receiveTimeout=3000;
      response = await dio.post(CREATE_UPLOAD_IMG_URL,
          data: {"image": base64Image},
          options: Options(headers: {
            'Authorization': prefs.getString('token_type') +
                ' ' + prefs.getString('access_token'), // set content-length
          }));
    } catch (e) {
      print(e);
    }
  }

  static void reviewImg() async {
    List<String> list = new List();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final CREATE_GET_URL = getip()+'/api/auth/reviewimg';
    Response response;
    Dio dio = new Dio();
    dio.options.connectTimeout = 10000; //5s
    dio.options.receiveTimeout=3000;
    response = await dio.get(CREATE_GET_URL,options: Options(headers: {
      'Authorization': prefs.getString('token_type') +
          ' ' + prefs.getString('access_token'), // set content-length
    }));
    globals.photo_list = response.data['photo_list'];
    globals.small_photo_list = response.data['small_photo_list'];
    globals.exchanged_list = response.data['exchanged_list'];
    globals.lockedPhotos = response.data['locked_photos'];
  }

  static void downloadImg(downloadlink) async {
    final  url =  getip()+'/api/auth/downloadimg/'+downloadlink;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static void updateScores(scores) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String CREATE_UPDATE_SCORE_URL =
        getip()+'/api/auth/updatescores';
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.connectTimeout = 10000; //5s
      dio.options.receiveTimeout=3000;
      response = await dio.post(CREATE_UPDATE_SCORE_URL,
          data: {"scores": scores},
          options: Options(headers: {
            'Authorization': prefs.getString('token_type') +
                ' ' + prefs.getString('access_token'), // set content-length
          }));
      getUserInfo();
    } catch (e) {
      print(e);
    }
  }

  static void unlockImg(count) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String CREATE_UNLCOK_IMG_URL =
        getip()+'/api/auth/unlockimgs';
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.connectTimeout = 10000; //5s
      dio.options.receiveTimeout=3000;
      response = await dio.post(CREATE_UNLCOK_IMG_URL,
          data: {"count": count},
          options: Options(headers: {
            'Authorization': prefs.getString('token_type') +
                ' ' + prefs.getString('access_token'), // set content-length
          }));
      reviewImg();
    } catch (e) {
      print(e);
    }
  }

  static void getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final CREATE_GET_USER_INFO_URL = getip()+'/api/auth/getuserinfo';
    Response response;
    Dio dio = new Dio();
    dio.options.connectTimeout = 10000; //5s
    dio.options.receiveTimeout=3000;
    response = await dio.get(CREATE_GET_USER_INFO_URL,options: Options(headers: {
      'Authorization': prefs.getString('token_type') +
          ' ' + prefs.getString('access_token'), // set content-length
    }));
    globals.name = response.data['name'];
    globals.scores = response.data['scores'];
  }
}
