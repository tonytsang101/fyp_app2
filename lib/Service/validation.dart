class Validations {
  String validateUsername(String value) {
    if (value.isEmpty) return 'Required*';
    final RegExp nameExp = new RegExp(r'^[A-za-z0-9_]+$');
    if (!nameExp.hasMatch(value))
      return 'Invalid Username';
    return null;
  }

  String validateName(String value) {
    if (value.isEmpty) return 'Required*';
    final RegExp nameExp = new RegExp(r'^[A-za-z \u4e00-\u9fa5]+$');
    if (!nameExp.hasMatch(value))
      return 'Please enter alphabetical characters.';
    return null;
  }

  String validatePhone(String value) {
    if (value.isEmpty) return 'Required*';
    final RegExp nameExp = new RegExp(r'^[0-9]+$');
    if (!nameExp.hasMatch(value)&&!value.isEmpty) return 'Please enter numbers.';
    return null;
  }

  String validatePassword(String value) {
    if (value.isEmpty) return 'Required*';
    return null;
  }

  String validatePasswordConfirmation(String value, String password) {
    if (value.isEmpty) return 'Required*';
    if (value != password) return 'Not same with password';
    return null;
  }
}
