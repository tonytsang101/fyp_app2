import 'dart:async';

import 'package:flutter/material.dart';

/// This class is generated by the flappy_translator package
/// Please do not change anything manually in this file, instead re-generate it when changes are available
class I18n {
  String get language => _getText("language");

  String get login => _getText("login");

  String get logout => _getText("logout");

  String get loginSuccess => _getText("loginSuccess");

  String get loginFail => _getText("loginFail");

  String get confirm => _getText("confirm");

  String get memberId => _getText("memberId");

  String get password => _getText("password");

  String get confirmPassword => _getText("confirmPassword");

  String get rememberMe => _getText("rememberMe");

  String get newMember => _getText("newMember");

  String get registration => _getText("registration");

  String get name => _getText("name");

  String get contactNum => _getText("contactNum");

  String get phone => _getText("phone");

  String get game => _getText("game");

  String get matchingGame => _getText("matchingGame");

  String get puzzleGame => _getText("puzzleGame");

  String get myPhoto => _getText("myPhoto");

  String get leave => _getText("leave");

  String get yes => _getText("yes");

  String get no => _getText("no");

  String get gameRule => _getText("gameRule");

  String get matchingGameInstructionA => _getText("matchingGameInstructionA");

  String get matchingGameInstructionB => _getText("matchingGameInstructionB");

  String get matchingGameInstructionC => _getText("matchingGameInstructionC");

  String get matchingGameInstruction1 => _getText("matchingGameInstruction1");

  String get matchingGameInstruction2 => _getText("matchingGameInstruction2");

  String get matchingGameInstruction3 => _getText("matchingGameInstruction3");

  String get start => _getText("start");

  String get timesUp => _getText("timesUp");

  String get youHaveGot => _getText("youHaveGot");

  String get coins => _getText("coins");

  String get coinsShow => _getText("coinsShow");

  String get collect => _getText("collect");

  String get timeLeft => _getText("timeLeft");

  String get score => _getText("score");

  String get hello => _getText("hello");

  String get rewards => _getText("rewards");

  String get getRewardsCompleted => _getText("getRewardsCompleted");

  String get getRewards => _getText("getRewards");

  String get back => _getText("back");

  String get download => _getText("download");

  String get difficulty => _getText("difficulty");

  String get game1Lv1 => _getText("game1Lv1");

  String get game1Lv2 => _getText("game1Lv2");

  String get game1Lv3 => _getText("game1Lv3");

  String get puzzleGameInstruction1 => _getText("puzzleGameInstruction1");

  String get puzzleGameInstruction2 => _getText("puzzleGameInstruction2");

  String get puzzleGameInstruction3 => _getText("puzzleGameInstruction3");

  static Map<String, String> _localizedValues;

  static Map<String, String> _enValues = {
    "language": "繁",
    "login": "Login",
    "logout": "Log Out",
    "loginSuccess": "Login Success!",
    "loginFail": "Login Fail!",
    "confirm": "OK",
    "memberId": "Member ID",
    "password": "Password",
    "confirmPassword": "Confirm Password",
    "rememberMe": "Remember Me",
    "newMember": "New Member? ",
    "registration": "Registration",
    "name": "Name",
    "contactNum": "Contact No.",
    "phone": "Phone number",
    "game": "Game",
    "matchingGame": "Matching Game",
    "puzzleGame": "Number Puzzle Game",
    "myPhoto": "My Photo",
    "leave": "Leaving?",
    "yes": "Yes",
    "no": "No",
    "gameRule": "Game Rule",
    "matchingGameInstructionA":
        "1 minutes game\nChoose a pair of cards with the same number and color\n\nCorrect: get 1 scores\nWrong: decrease 0 scores\n\n>15 scores = 12 coins\n10-15 scores = 9 coins\n5-10 scores = 6 coins\n0-15 scores = 3 coins",
    "matchingGameInstructionB":
        "1 minutes game\nChoose a pair of cards with the same number and color\n\nCorrect: get 2 scores\nWrong: decrease 1 scores\n\n>40 scores = 20 coins\n25-40 scores = 15 coins\n10-25 scores = 10 coins\n0-10 scores = 5 coins",
    "matchingGameInstructionC":
        "1 minutes game\nChoose a pair of cards with the same number and color\n\nCorrect: get 3 scores\nWrong: decrease 2 scores\n\n>50 scores = 40 coins\n30-50 scores = 30 coins\n15-30 scores = 20 coins\n0-15 scores = 10 coins",
    "matchingGameInstruction1":
        "1 minutes game\nChoose a pair of cards with the same number and color\n\nCorrect: get corresponding number of scores\nWrong: decrease 1 scores\n\n>225 scores = 12 coins\n150-225 scores = 9 coins\n75-150 scores = 6 coins\n0-75 scores = 3 coins",
    "matchingGameInstruction2":
        "1 minutes game\nChoose a pair of cards with the same number and color\n\nCorrect: get corresponding number of scores\nWrong: decrease 5 scores\n\n>300 scores = 20 coins\n200-300 scores = 15 coins\n100-200 scores = 10 coins\n 0-100 scores = 5 coins",
    "matchingGameInstruction3":
        "1 minutes game\nChoose a pair of cards with the same number and color\n\nCorrect: get corresponding number of scores\nWrong: decrease 10 scores\n\n>450 scores = 40 coins\n300-450 scores = 30 coins\n150-300 scores = 20 coins\n0-150 scores = 10 coins",
    "start": "START",
    "timesUp": "Times Up!",
    "youHaveGot": "You have got",
    "coins": "coins",
    "coinsShow": "          Coins:",
    "collect": "Collect",
    "timeLeft": "Time Left:",
    "score": "Score:",
    "hello": "Hello!",
    "rewards": "Rewards",
    "getRewardsCompleted": "Completed",
    "getRewards": "Get rewards",
    "back": "Back",
    "download": "Download",
    "difficulty": "Difficulty",
    "game1Lv1": "Simple",
    "game1Lv2": "Normal",
    "game1Lv3": "Hard",
    "puzzleGameInstruction1":
        "1 minutes game\nChoose the correct numbers or symbols to finish the equation\n\nCorrect: get 1 scores\nWrong: decrease 0 scores\n\n>15 scores = 12 coins\n10-15 scores = 9 coins\n5-10 scores = 6 coins\n0-15 scores = 3 coins",
    "puzzleGameInstruction2":
        "1 minutes game\nChoose the correct numbers or symbols to finish the equation\n\nCorrect: get 2 scores\nWrong: decrease 1 scores\n\n>40 scores = 20 coins\n25-40 scores = 15 coins\n10-25 scores = 10 coins\n0-10 scores = 5 coins",
    "puzzleGameInstruction3":
        "1 minutes game\nChoose the correct numbers or symbols to finish the equation\n\nCorrect: get 3 scores\nWrong: decrease 2 scores\n\n>50 scores = 40 coins\n30-50 scores = 30 coins\n15-30 scores = 20 coins\n0-15 scores = 10 coins",
  };

  static Map<String, String> _zhValues = {
    "language": "EN",
    "login": "登入",
    "logout": "登出",
    "loginSuccess": "登入成功!",
    "loginFail": "登入失敗!\n請檢查你的帳戶或密碼…",
    "confirm": "確認",
    "memberId": "會員編號",
    "password": "密碼",
    "confirmPassword": "確認密碼",
    "rememberMe": "記住我",
    "newMember": "新朋友? ",
    "registration": "註冊",
    "name": "姓名",
    "contactNum": "聯絡電話",
    "phone": "電話",
    "game": "遊戲",
    "matchingGame": "配對遊戲",
    "puzzleGame": "數字拼圖遊戲",
    "myPhoto": "我的相片",
    "leave": "是否離開?",
    "yes": "是",
    "no": "否",
    "gameRule": "遊戲玩法",
    "matchingGameInstructionA":
        "限時一分鐘\n選擇相同數字及顏色的卡\n\n正確: 獲得1分\n錯誤: 不扣分\n\n15分以上 = 12個金幣\n10-15分 = 9個金幣\n5-10分 = 6個金幣\n0-5分 = 3個金幣",
    "matchingGameInstructionB":
        "限時一分鐘\n選擇相同數字及顏色的卡\n\n正確: 獲得2分\n錯誤: 扣1分\n\n40分以上 = 20個金幣\n25-40分 = 15個金幣\n10-25分 = 10個金幣\n0-10分 = 5個金幣",
    "matchingGameInstructionC":
        "限時一分鐘\n選擇相同數字及顏色的卡\n\n正確: 獲得3分\n錯誤: 扣2分\n\n50分以上 = 40個金幣\n30-50分 = 30個金幣\n15-30分 = 20個金幣\n0-15分 = 10個金幣",
    "matchingGameInstruction1":
        "限時一分鐘\n選擇相同數字及顏色的卡\n\n正確: 獲得卡上的分數\n錯誤: 扣1分\n\n225分以上 = 12個金幣\n150-225分 = 9個金幣\n75-150分 = 6個金幣\n0-75分 = 3個金幣",
    "matchingGameInstruction2":
        "限時一分鐘\n選擇相同數字及顏色的卡\n\n正確: 獲得卡上的分數\n錯誤: 扣5分\n\n300分以上 = 20個金幣\n200-300分 = 15個金幣\n100-200分 = 10個金幣\n0-100分 = 5個金幣",
    "matchingGameInstruction3":
        "限時一分鐘\n選擇相同數字及顏色的卡\n\n正確: 獲得卡上的分數\n錯誤: 扣10分\n\n450分以上 = 40個金幣\n300-450分 = 30個金幣\n150-300分 = 20個金幣\n0-150分 = 10個金幣",
    "start": "開始",
    "timesUp": "時間夠!",
    "youHaveGot": "你獲得金幣",
    "coins": "個",
    "coinsShow": "          金幣:",
    "collect": "領取",
    "timeLeft": "剩餘時間:",
    "score": "分數:",
    "hello": "你好!",
    "rewards": "獎勵",
    "getRewardsCompleted": "兌換完成",
    "getRewards": "兌換獎勵",
    "back": "返回",
    "download": "下載圖片",
    "difficulty": "選擇難度",
    "game1Lv1": "簡單",
    "game1Lv2": "普通",
    "game1Lv3": "困難",
    "puzzleGameInstruction1":
        "限時一分鐘\n選擇合適的數字或符號使算式成立\n\n正確: 獲得1分\n錯誤: 不扣分\n\n15分以上 = 12個金幣\n10-15分 = 9個金幣\n5-10分 = 6個金幣\n0-5分 = 3個金幣",
    "puzzleGameInstruction2":
        "限時一分鐘\n選擇合適的數字或符號使算式成立\n\n正確: 獲得2分\n錯誤: 扣1分\n\n40分以上 = 20個金幣\n25-40分 = 15個金幣\n10-25分 = 10個金幣\n0-10分 = 5個金幣",
    "puzzleGameInstruction3":
        "限時一分鐘\n選擇合適的數字或符號使算式成立\n\n正確: 獲得3分\n錯誤: 扣2分\n\n50分以上 = 40個金幣\n30-50分 = 30個金幣\n15-30分 = 20個金幣\n0-15分 = 10個金幣",
  };

  static Map<String, Map<String, String>> _allValues = {
    "en": _enValues,
    "zh": _zhValues,
  };

  I18n(Locale locale) {
    this._locale = locale;
    _localizedValues = null;
  }

  Locale _locale;

  static I18n of(BuildContext context) {
    return Localizations.of<I18n>(context, I18n);
  }

  String _getText(String key) {
    return _localizedValues[key] ?? '** $key not found';
  }

  Locale get currentLocale => _locale;

  String get currentLanguage => _locale.languageCode;

  static Future<I18n> load(Locale locale) async {
    final translations = I18n(locale);
    _localizedValues = _allValues[locale.toString()];
    return translations;
  }
}

class I18nDelegate extends LocalizationsDelegate<I18n> {
  const I18nDelegate();

  static final Set<Locale> supportedLocals = {
    Locale('en'),
    Locale('zh'),
  };

  @override
  bool isSupported(Locale locale) => supportedLocals.contains(locale);

  @override
  Future<I18n> load(Locale locale) => I18n.load(locale);

  @override
  bool shouldReload(I18nDelegate old) => false;
}
